use rand::{seq::SliceRandom, Rng};
use raptorq::{Decoder, Encoder, EncodingPacket, ObjectTransmissionInformation};

fn main() {
    let rng = &mut rand::thread_rng();
    let len = 20_000;
    let mut v = Vec::with_capacity(len);

    for _ in 0..len {
        v.push(rng.gen::<u8>());
    }
    let config = ObjectTransmissionInformation::with_defaults(len as u64, 1200);
    let encoder = Encoder::new(&v, config);
    let mut packets = encoder
        .get_encoded_packets(10)
        .iter()
        .map(|p| p.serialize())
        .collect::<Vec<_>>();

    packets.shuffle(rng);

    let mut decoder = Decoder::new(config);

    let mut v2 = None;
    for (i, p) in packets.iter().enumerate() {
        v2 = decoder.decode(EncodingPacket::deserialize(p));
        if v2.is_some() {
            println!(
                "recovered after {i} packets received, out of {} total",
                packets.len()
            );
            break;
        }
    }

    assert!(v2.is_some());
    assert_eq!(v2.unwrap(), v);
}
