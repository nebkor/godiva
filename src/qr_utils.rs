use fast_qr::convert::image::ImageBuilder;

use crate::{stream_bytes, Content, StreamedContent};

/// Makes a PNG of a QR code for the given bytes, returns the bytes of the PNG.
pub fn mk_qr_bytes(bytes: &[u8], height: f32) -> Vec<u8> {
    let qr = fast_qr::QRBuilder::new(bytes)
        .ecl(fast_qr::ECL::L)
        .build()
        .unwrap();

    ImageBuilder::default()
        .fit_width(height as u32)
        .to_pixmap(&qr)
        .encode_png()
        .unwrap()
}

/// Turns bytes and a description into either a single QR code, or a stream of
/// them, depending on the size of the input.
pub fn get_content(bytes: Vec<u8>, desc: &str, filename: Option<&str>) -> Content {
    if bytes.len() < 2000 && fast_qr::QRBuilder::new(bytes.clone()).build().is_ok() {
        let bytes = bytes.leak();
        Content::Static(bytes)
    } else {
        let (tx, rx) = std::sync::mpsc::sync_channel(2);
        let txconfig = stream_bytes(bytes, tx, desc, filename).leak();
        let stream = StreamedContent::new(txconfig, rx);
        Content::Streamed(stream)
    }
}
