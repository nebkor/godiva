use std::ffi::OsString;

use clap::Parser;
use cuttle::{get_content, Flasher};
use eframe::egui;

#[derive(Parser, Debug)]
#[clap(author, version, trailing_var_arg = true)]
struct Cli {
    #[clap(long, short, help = "File to expose")]
    pub file: Option<OsString>,

    #[clap(long, help = "Frames per second", default_value_t = 10.0)]
    pub fps: f64,

    #[clap(
        help = "all remaining arguments treated as a string; this string is the whole message if `-f` is not given, otherwise it's an optional description of the file"
    )]
    text: Vec<String>,
}

impl Cli {
    pub fn text(&self) -> &Vec<String> {
        &self.text
    }
}

fn main() -> Result<(), eframe::Error> {
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).

    let cli = Cli::parse();

    let (description, filename) = if let Some(ref file) = cli.file {
        let text = cli.text().join(" ");
        let sep = if text.is_empty() { "" } else { ": " };
        let file = std::path::Path::new(&file)
            .file_name()
            .unwrap_or_default()
            .to_string_lossy()
            .to_string();
        (format!("{file}{sep}{text}"), Some(file))
    } else {
        ("text message".to_string(), None)
    };

    let bytes = if let Some(ref file) = cli.file {
        std::fs::read(file).unwrap_or_else(|e| panic!("tried to open {file:?}, got {e:?}"))
    } else {
        cli.text().join(" ").bytes().collect()
    };

    let content = get_content(bytes, &description, filename.as_deref());

    let flasher = Flasher::new(description, content, cli.fps);

    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(1200.0, 1200.0)),
        active: true,
        vsync: true,
        ..Default::default()
    };

    eframe::run_native(
        "Cuttle: optical insanity",
        options,
        Box::new(move |_cc| Box::new(flasher)),
    )
}
