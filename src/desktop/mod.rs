use std::{
    sync::mpsc::{channel, Sender},
    time::{Duration, Instant},
};

use eframe::{
    egui::{self, Direction, Layout, RichText, Ui},
    epaint::FontId,
};
use egui_extras::{RetainedImage, Size, StripBuilder};

use crate::{mk_qr_bytes, Content, Flasher, StreamStatus, StreamedContent};

const TEXT_FACTOR: f32 = 0.05;
const IMG_AREA_PCT: f32 = 0.85;

struct Button<'text> {
    pub text: &'text str,
    pub next_state: StreamStatus,
}

impl eframe::App for Flasher {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        ctx.request_repaint_after(self.sleep);
        let height = ctx.screen_rect().height();
        let tsize = (height * TEXT_FACTOR) - 10.0;
        let streaming_height = (height * IMG_AREA_PCT) - 50.0;
        let static_height = height - 50.0;
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading(&self.description);
            match self.content {
                Content::Static(bytes) => {
                    let img = RetainedImage::from_image_bytes(
                        "static content",
                        &mk_qr_bytes(bytes, static_height),
                    )
                    .unwrap();

                    // If it's static, just show the thing, don't set up any other UI or buttons or
                    // anything
                    img.show(ui);
                }
                Content::Streamed(ref mut streamed_content) => match streamed_content.status {
                    StreamStatus::Paused => {
                        paused(streamed_content, ui, streaming_height, tsize);
                    }
                    StreamStatus::Streaming => {
                        streaming(streamed_content, ui, streaming_height, tsize, self.sleep);
                    }
                },
            }
            // check for quit key
            if ui.input(|i| i.key_pressed(egui::Key::Q))
                || ui.input(|i| i.key_pressed(egui::Key::Escape))
            {
                frame.close();
            }
        });
    }
}

fn paused(sc: &mut StreamedContent, ui: &mut Ui, height: f32, tsize: f32) {
    let img = RetainedImage::from_image_bytes(
        "tx config for receiver initialization",
        &mk_qr_bytes(sc.txconfig, height),
    )
    .unwrap();
    let (tx, rx) = channel();
    let button = Button {
        text: "Scan, then click to begin streaming",
        next_state: StreamStatus::Streaming,
    };

    render_streaming_ui(ui, button, tx, img, tsize);
    if let Ok(new_status) = rx.try_recv() {
        sc.status = new_status;
    }
    sc.last_packet_time = Instant::now();
}

fn streaming(sc: &mut StreamedContent, ui: &mut Ui, height: f32, tsize: f32, sleep: Duration) {
    let dur = Instant::now() - sc.last_packet_time;
    // don't grab a new packet unless enough time has passed or we've never seen a
    // packet before
    let img = if dur < sleep && sc.last_packet.is_some() {
        let bytes = sc.last_packet.clone().unwrap();
        RetainedImage::from_image_bytes("last packet", &mk_qr_bytes(&bytes, height)).unwrap()
    } else {
        let bytes = sc.rx.recv().unwrap();
        sc.last_packet = Some(bytes.clone());
        sc.last_packet_time = Instant::now();
        RetainedImage::from_image_bytes("new packet", &mk_qr_bytes(&bytes, height)).unwrap()
    };

    let (tx, rx) = channel();
    let button = Button {
        text: "pause and show txconfig",
        next_state: StreamStatus::Paused,
    };
    render_streaming_ui(ui, button, tx, img, tsize);
    if let Ok(new_status) = rx.try_recv() {
        sc.status = new_status;
    }
}

fn render_streaming_ui(
    ui: &mut Ui,
    button: Button,
    sender: Sender<StreamStatus>,
    image: RetainedImage,
    tsize: f32,
) {
    StripBuilder::new(ui)
        .size(Size::relative(0.10))
        .size(Size::remainder())
        .cell_layout(Layout::centered_and_justified(Direction::TopDown))
        .vertical(|mut strip| {
            strip.strip(|pstrip| {
                pstrip.sizes(Size::remainder(), 1).horizontal(|mut pstrip| {
                    pstrip.cell(|ui| {
                        let button_text = RichText::new(button.text).font(FontId::monospace(tsize));
                        if ui.button(button_text.clone()).clicked() {
                            sender.send(button.next_state).unwrap();
                        }
                    });
                });
            });
            strip.cell(|ui| {
                image.show(ui);
            });
        });
}
