use std::{
    fmt::Debug,
    time::{Duration, Instant},
};

use rand::seq::SliceRandom;
use raptorq::{Encoder, ObjectTransmissionInformation};
use rkyv::{Archive, Deserialize, Serialize};

#[cfg(feature = "desktop")]
mod desktop;
#[cfg(feature = "desktop")]
mod qr_utils;

#[cfg(feature = "desktop")]
pub use qr_utils::{get_content, mk_qr_bytes};

pub type CuttleSender = std::sync::mpsc::SyncSender<Vec<u8>>;
pub type CuttleReceiver = std::sync::mpsc::Receiver<Vec<u8>>;
pub const STREAMING_MTU: u16 = 1200;

/// The application state
#[derive(Debug)]
pub struct Flasher {
    pub description: String,
    pub content: Content,
    pub sleep: Duration,
}

impl Flasher {
    pub fn new(description: String, content: Content, fps: f64) -> Self {
        let sleep = 1000.0 / fps;
        let sleep = Duration::from_millis(sleep as u64);
        Flasher {
            description,
            content,
            sleep,
        }
    }
}

#[derive(Debug)]
pub enum Content {
    Static(&'static [u8]),
    Streamed(StreamedContent),
}

#[derive(Debug)]
pub struct StreamedContent {
    pub txconfig: &'static [u8],
    pub rx: CuttleReceiver,
    pub status: StreamStatus,
    pub last_packet: Option<Vec<u8>>,
    pub last_packet_time: Instant,
}

impl StreamedContent {
    pub fn new(txconfig: &'static [u8], rx: CuttleReceiver) -> Self {
        StreamedContent {
            txconfig,
            rx,
            status: StreamStatus::Paused,
            last_packet: None,
            last_packet_time: Instant::now() - Duration::from_secs(1),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum StreamStatus {
    Paused,
    Streaming,
}

#[derive(Debug, Clone, PartialEq, Eq, Archive, Serialize, Deserialize)]
#[archive(check_bytes)]
pub struct TxConfig {
    pub len: u64,
    pub mtu: u16,
    pub description: String,
    pub filename: Option<String>,
}

pub fn stream_bytes(
    bytes: Vec<u8>,
    tx: CuttleSender,
    desc: &str,
    filename: Option<&str>,
) -> Vec<u8> {
    let len = bytes.len() as u64;

    let rng = &mut rand::thread_rng();

    let config = ObjectTransmissionInformation::with_defaults(len, STREAMING_MTU);
    let encoder = Encoder::new(&bytes, config);

    let mut packets = encoder
        .get_encoded_packets(10)
        .iter()
        .map(|p| p.serialize())
        .collect::<Vec<_>>();

    packets.shuffle(rng);

    std::thread::spawn(move || {
        for packet in packets.iter().cycle() {
            tx.send(packet.clone()).unwrap_or_default();
        }
    });

    let txconfig = TxConfig {
        len,
        mtu: STREAMING_MTU,
        description: desc.to_string(),
        filename: filename.map(|f| f.to_string()),
    };
    rkyv::to_bytes::<_, 256>(&txconfig)
        .expect("tried to serialize the txconfig")
        .to_vec()
}
