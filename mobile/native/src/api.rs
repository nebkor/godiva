use raptorq::{Decoder, EncodingPacket, ObjectTransmissionInformation};

#[derive(Debug, Clone)]
pub struct TxConfig {
    pub len: u64,
    pub mtu: u16,
    pub description: String,
    pub filename: Option<String>,
}

#[derive(Debug, Clone)]
pub struct RaptorPacket(pub Vec<u8>);

impl From<cuttle::TxConfig> for TxConfig {
    fn from(value: cuttle::TxConfig) -> Self {
        TxConfig {
            len: value.len,
            mtu: value.mtu,
            description: value.description,
            filename: value.filename,
        }
    }
}

pub fn get_tx_config(bytes: Vec<u8>) -> Option<TxConfig> {
    if let Ok(archive) = rkyv::check_archived_root::<cuttle::TxConfig>(&bytes) {
        <cuttle::ArchivedTxConfig as rkyv::Deserialize<cuttle::TxConfig, rkyv::Infallible>>::deserialize(archive, &mut rkyv::Infallible)
            .ok().map(Into::into)
    } else {
        None
    }
}

pub fn drop_tx_config(_txc: TxConfig) {}

pub fn decode_packets(packets: Vec<RaptorPacket>, txconf: TxConfig) -> Option<Vec<u8>> {
    let conf = ObjectTransmissionInformation::with_defaults(txconf.len, txconf.mtu);
    let mut decoder = Decoder::new(conf);

    let mut res = None;
    for RaptorPacket(p) in &packets {
        res = decoder.decode(EncodingPacket::deserialize(p));
        if res.is_some() {
            break;
        }
    }
    res
}
