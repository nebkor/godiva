use super::*;
// Section: wire functions

#[no_mangle]
pub extern "C" fn wire_get_tx_config(port_: i64, bytes: *mut wire_uint_8_list) {
    wire_get_tx_config_impl(port_, bytes)
}

#[no_mangle]
pub extern "C" fn wire_drop_tx_config(port_: i64, _txc: *mut wire_TxConfig) {
    wire_drop_tx_config_impl(port_, _txc)
}

#[no_mangle]
pub extern "C" fn wire_decode_packets(
    port_: i64,
    packets: *mut wire_list_raptor_packet,
    txconf: *mut wire_TxConfig,
) {
    wire_decode_packets_impl(port_, packets, txconf)
}

// Section: allocate functions

#[no_mangle]
pub extern "C" fn new_box_autoadd_tx_config_0() -> *mut wire_TxConfig {
    support::new_leak_box_ptr(wire_TxConfig::new_with_null_ptr())
}

#[no_mangle]
pub extern "C" fn new_list_raptor_packet_0(len: i32) -> *mut wire_list_raptor_packet {
    let wrap = wire_list_raptor_packet {
        ptr: support::new_leak_vec_ptr(<wire_RaptorPacket>::new_with_null_ptr(), len),
        len,
    };
    support::new_leak_box_ptr(wrap)
}

#[no_mangle]
pub extern "C" fn new_uint_8_list_0(len: i32) -> *mut wire_uint_8_list {
    let ans = wire_uint_8_list {
        ptr: support::new_leak_vec_ptr(Default::default(), len),
        len,
    };
    support::new_leak_box_ptr(ans)
}

// Section: related functions

// Section: impl Wire2Api

impl Wire2Api<String> for *mut wire_uint_8_list {
    fn wire2api(self) -> String {
        let vec: Vec<u8> = self.wire2api();
        String::from_utf8_lossy(&vec).into_owned()
    }
}
impl Wire2Api<TxConfig> for *mut wire_TxConfig {
    fn wire2api(self) -> TxConfig {
        let wrap = unsafe { support::box_from_leak_ptr(self) };
        Wire2Api::<TxConfig>::wire2api(*wrap).into()
    }
}
impl Wire2Api<Vec<RaptorPacket>> for *mut wire_list_raptor_packet {
    fn wire2api(self) -> Vec<RaptorPacket> {
        let vec = unsafe {
            let wrap = support::box_from_leak_ptr(self);
            support::vec_from_leak_ptr(wrap.ptr, wrap.len)
        };
        vec.into_iter().map(Wire2Api::wire2api).collect()
    }
}

impl Wire2Api<RaptorPacket> for wire_RaptorPacket {
    fn wire2api(self) -> RaptorPacket {
        RaptorPacket(self.field0.wire2api())
    }
}
impl Wire2Api<TxConfig> for wire_TxConfig {
    fn wire2api(self) -> TxConfig {
        TxConfig {
            len: self.len.wire2api(),
            mtu: self.mtu.wire2api(),
            description: self.description.wire2api(),
            filename: self.filename.wire2api(),
        }
    }
}

impl Wire2Api<Vec<u8>> for *mut wire_uint_8_list {
    fn wire2api(self) -> Vec<u8> {
        unsafe {
            let wrap = support::box_from_leak_ptr(self);
            support::vec_from_leak_ptr(wrap.ptr, wrap.len)
        }
    }
}
// Section: wire structs

#[repr(C)]
#[derive(Clone)]
pub struct wire_list_raptor_packet {
    ptr: *mut wire_RaptorPacket,
    len: i32,
}

#[repr(C)]
#[derive(Clone)]
pub struct wire_RaptorPacket {
    field0: *mut wire_uint_8_list,
}

#[repr(C)]
#[derive(Clone)]
pub struct wire_TxConfig {
    len: u64,
    mtu: u16,
    description: *mut wire_uint_8_list,
    filename: *mut wire_uint_8_list,
}

#[repr(C)]
#[derive(Clone)]
pub struct wire_uint_8_list {
    ptr: *mut u8,
    len: i32,
}

// Section: impl NewWithNullPtr

pub trait NewWithNullPtr {
    fn new_with_null_ptr() -> Self;
}

impl<T> NewWithNullPtr for *mut T {
    fn new_with_null_ptr() -> Self {
        std::ptr::null_mut()
    }
}

impl NewWithNullPtr for wire_RaptorPacket {
    fn new_with_null_ptr() -> Self {
        Self {
            field0: core::ptr::null_mut(),
        }
    }
}

impl Default for wire_RaptorPacket {
    fn default() -> Self {
        Self::new_with_null_ptr()
    }
}

impl NewWithNullPtr for wire_TxConfig {
    fn new_with_null_ptr() -> Self {
        Self {
            len: Default::default(),
            mtu: Default::default(),
            description: core::ptr::null_mut(),
            filename: core::ptr::null_mut(),
        }
    }
}

impl Default for wire_TxConfig {
    fn default() -> Self {
        Self::new_with_null_ptr()
    }
}

// Section: sync execution mode utility

#[no_mangle]
pub extern "C" fn free_WireSyncReturn(ptr: support::WireSyncReturn) {
    unsafe {
        let _ = support::box_from_leak_ptr(ptr);
    };
}
