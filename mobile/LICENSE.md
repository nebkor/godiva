This portion of the software is covered under the terms of the [Chaos
License](../LICENSE.md). Portions of the code in this mobile module are used under the terms of the
[MIT License](./LICENSE-MIT).
