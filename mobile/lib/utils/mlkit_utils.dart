import 'package:camerawesome/camerawesome_plugin.dart';
import 'package:google_mlkit_commons/google_mlkit_commons.dart';

InputImage toInputImage(Nv21Image aimg) {
  final planeData = aimg.planes
      .map((e) => InputImagePlaneMetadata(
          bytesPerRow: e.bytesPerRow, height: aimg.height, width: aimg.width))
      .toList();

  return InputImage.fromBytes(
    bytes: aimg.bytes,
    inputImageData: InputImageData(
      imageRotation: getRotation(aimg),
      inputImageFormat: InputImageFormat.nv21,
      planeData: planeData,
      size: aimg.size,
    ),
  );
}

InputImageRotation getRotation(AnalysisImage img) {
  return InputImageRotation.values.byName(img.rotation.name);
}
