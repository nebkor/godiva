# Cuttle, a flasher with a message

Cuttle is a desktop application for transfering data from one computer to another by displaying a
[possibly endless] stream of [QR encoded](https://en.wikipedia.org/wiki/QR_code) [Raptor
codes](https://en.wikipedia.org/wiki/Raptor_code), so that a camera-equipped different computer can
read the QR codes and turn them back into the original content.

## Tell me more!

Raptor codes are a type of [fountain code](https://en.wikipedia.org/wiki/Fountain_code); fountain
codes are a way to encode data as a stream of error-correcting pieces of that data; as long as you
can read some number of pieces, you can reconstruct the original data. They're named like that
because you can think of them as like a fountain of water that is just pouring out. Anyone can come
drink from it, and then they'll have the water. So something can just be transmitting fountain codes
and not care if anyone is receiving, and receivers don't have to worry about missing something they
needed; they're guaranteed to get everything they need as long as they listen long enough. I think
that's pretty cool!

So the idea here is to give it a file (or short string of text) that you want transferred to a
different computer (like a mobile phone running a little companion app), and it will start producing
Raptor codes for that file. Each piece of Raptor code then gets encoded as a QR code, and then
displayed for a period of time on the screen. The user then holds up the receiving computer's camera
in order to receive the QR encoded raptor codes, and, *voila*, the file has been transferred!

# Current status (2023-08-20)

Currently, the following is done in terms of functionality:

**Desktop**
 - [x] transmit short text given on the commandline with a single qr code
 - [x] transmit large text given on the commandline with a stream of qr codes
 - [x] transmit small files given on the commandline with a single qr code
 - [x] transmit large files given on the commandline with a stream of qr codes
 - [ ] receive text or files with a single qr code
 - [ ] receive text or files with a stream of qr codes

**Android app**
 - [ ] transmit short given text with a single qr code
 - [ ] transmit large given text with a stream of qr codes
 - [ ] transmit small files with a single qr code
 - [ ] transmit large files with a stream of qr codes
 - [x] receive text or files with a single qr code
 - [x] receive text or files with a stream of qr codes

The desktop app (unix-ish only; developed on linux, untested on anything else, but should run on macos)
is closer to its final form in terms of UI/UX, though it currently can only transmit. The Android
app is about halfway done in terms of functionality, but the UI is, shall we say, "bad":

![cuttle mobile app receiving data](./mobile_receiving.png)

I might take a crack at making it less hideous before I start on the mobile transmitting/desktop
receiving side of the functionality, but maybe not! I often want to transfer a file or text from my
phone to my desktop, and I'm willing to live with an ugly UI for a little bit.


# about the name

[Cuttlefish](https://en.wikipedia.org/wiki/Cuttlefish) are small but very intelligent cephalopods
whose bodies are covered in a 15hz multi-color, multi-polarity, pixel-mapped display that they use
to visually communicate with other cuttlefish, as well as to dazzle predators and prey.
